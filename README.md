# Minesweeper

## Depencies
* Make
* GNU C++ Compiler

## Compile
To compile the program, run `make`.

## Input/Output
The program takes input from stdin and outputs to stdout.

## Examples
Run examples using:
```bash
cat examples/example1.txt | ./Minesweeper.exe
cat examples/example2.txt | ./Minesweeper.exe
cat examples/example3.txt | ./Minesweeper.exe
```