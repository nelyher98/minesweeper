/*
    Minesweeper - Prints how many mines are touching each square.
    Copyright (C) 2022 Nely Hernandez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
using namespace std;

bool is_valid(int num_rows, int num_columns, int row, int column)
{
    return row >= 0 && row < num_rows && column >= 0 && column < num_columns;
}

int calc_bombs(char **board, int num_rows, int num_columns, int row, int column)
{
    // is a bomb
    if(board[row][column] == '*')
        return -1;

    int num_bombs = 0;

    // north
    if(is_valid(num_rows, num_columns, row - 1, column) && board[row - 1][column] == '*')
        num_bombs++;

    // north east
    if(is_valid(num_rows, num_columns, row - 1, column + 1) && board[row - 1][column + 1] == '*')
        num_bombs++;

    // east
    if(is_valid(num_rows, num_columns, row, column + 1) && board[row][column + 1] == '*')
        num_bombs++;

    // south east
    if(is_valid(num_rows, num_columns, row + 1, column + 1) && board[row + 1][column + 1] == '*')
        num_bombs++;

    // south
    if(is_valid(num_rows, num_columns, row + 1, column) && board[row + 1][column] == '*')
        num_bombs++;

    // south west
    if(is_valid(num_rows, num_columns, row + 1, column - 1) && board[row + 1][column - 1] == '*')
        num_bombs++;

    // west
    if(is_valid(num_rows, num_columns, row, column - 1) && board[row][column - 1] == '*')
        num_bombs++;

    // north west
    if(is_valid(num_rows, num_columns, row - 1, column - 1) && board[row - 1][column - 1] == '*')
        num_bombs++;

    return num_bombs;
}

int main()
{
    string s_num_rows;
    string s_num_columns;

    // get the integers
    getline(cin, s_num_rows, ' ');
    getline(cin, s_num_columns, '\n');

    // cast them into integers
    int num_rows = stoi(s_num_rows);
    int num_columns = stoi(s_num_columns);

    // initialize 2d array
    char **board = new char *[num_rows];
    for(int row = 0; row < num_rows; row++)
    {
        board[row] = new char[num_columns];
    }

    for(int row = 0; row < num_rows; row++)
    {
        for(int column = 0; column < num_columns; column++)
        {
            board[row][column] = getchar();
        }
        getchar();
    }

    // create the solution board
    char solution[num_rows][num_columns];
    int num_bombs;

    for(int row = 0; row < num_rows; row++)
    {
        for(int column = 0; column < num_columns; column++)
        {
            num_bombs = calc_bombs(board, num_rows, num_columns, row, column);

            if(num_bombs == -1)
            {
                solution[row][column] = '*';
            }
            else
            {
                solution[row][column] = num_bombs + 48; // 48 is the ASCII code for 0
            }
        }
    }

    // release 2d array board memory
    for(int row = 0; row < num_rows; row++)
    {
        delete board[row];
    }

    delete board;

    for(int row = 0; row < num_rows; row++)
    {
        for(int column = 0; column < num_columns; column++)
        {
            cout << solution[row][column];
        }
        cout << "\n";
    }

    return 0;
}
